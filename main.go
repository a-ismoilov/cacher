package main

import (
	"bufio"
	"flag"
	"fmt"
	"gitlab.com/swe_ismoilov/profiling/lfu"
	"log"
	"os"
	"os/signal"
	"runtime/pprof"
	"strings"
	"syscall"
)

var cpuProfile = flag.Bool("cpuprofile", false, "for enabling cpu profiling")

var memProfile = flag.Bool("memprofile", false, "for enabling memory profiling")

func main() {
	// parsing flags
	flag.Parse()

	// starting cpu profiling if flag specified
	if *cpuProfile {
		f, err := os.Create("cpu.pprof")
		if err != nil {
			log.Fatalf("cannot create cpu profiling file")
		}
		if err = pprof.StartCPUProfile(f); err != nil {
			log.Fatalf("cannot start cpu profiling")
		}
	}

	fmt.Println("A in-memory cache with Least Frequently Used (LFU) eviction algorithm with O(1) time complexity.\nTo stop the app press [ctrl + C]\nKey & Value must be solid string without any type of spaces")
	fmt.Println(" - To add value: $ add <key> <value>\n - To retrieve all keys: $ all\n - Number of the values: $ count\n - To retrieve value: $ get <key>")

	cache := lfu.New(-1)

	for {
		var command string
		fmt.Print("lfu>")

		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()

		inputs := strings.Split(scanner.Text(), " ")

		command = strings.ToLower(inputs[0])

		switch command {
		case "add":
			if len(inputs) != 3 {
				fmt.Println("invalid input format, should be: $ add <key> <value>")
				continue
			}

			cache.Add(inputs[1], inputs[2])
		case "get":
			if len(inputs) != 2 {
				fmt.Println("invalid input format, should be: $ get <key>")
				continue
			}

			value, exists := cache.Get(inputs[1])
			if !exists {
				fmt.Println("value does not exists")
				continue
			}

			fmt.Println(value)
		case "count":
			if len(inputs) != 1 {
				fmt.Println("invalid input format, should be: $ count")
				continue
			}

			fmt.Println(cache.Count())
		case "all":
			if len(inputs) != 1 {
				fmt.Println("invalid input format, should be: $ all")
				continue
			}

			for i := range cache.GetAllKeys() {
				fmt.Println(i)
			}
		default:
			fmt.Printf("command not found, [%s]\n", command)
		}
	}
}

func init() {
	// parsing flags
	flag.Parse()

	c := make(chan os.Signal)

	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-c

		// flush cpu profile data to cpu.pprof
		if *cpuProfile {
			pprof.StopCPUProfile()
		}

		// starting memory profiling if flag specified
		if *memProfile {
			f, err := os.Create("mem.pprof")
			if err != nil {
				log.Fatalf("cannot create memory profiling file")
			}

			if err = pprof.WriteHeapProfile(f); err != nil {
				log.Fatalf("cannot write to heap")
			}
		}

		os.Exit(0)
	}()
}
